package com.example.test.domain.products.update

import com.example.test.data.entites.ProductAdapterModel
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class UpdateProducts @Inject constructor() {
    fun updateProduct(
        data: List<ProductAdapterModel>,
        action: UpdateProductsAction
    ) = flow {
        val productAdapter = action.product
        val product = action.product.product

        val modifiedProduct = when (action) {
            is UpdateProductsAction.AddToBasket -> {
                productAdapter.copy(
                    addedToBasket = true, product = product.copy(count = product.count + 1)
                )
            }
            is UpdateProductsAction.Increase -> ProductAdapterModel.Product(
                product.copy(count = product.count + 1),
                productAdapter.addedToBasket
            )
            is UpdateProductsAction.Decrease -> ProductAdapterModel.Product(
                product.copy(count = product.count - 1),
                productAdapter.addedToBasket
            )
        }

        val indexOfOldItem = data.indexOf(productAdapter)

        val mutableData = data.toMutableList()

        if (indexOfOldItem >= 0) {
            mutableData.remove(productAdapter)
            mutableData.add(indexOfOldItem, modifiedProduct)
        }

        emit(UpdateProductsResult(action, mutableData))
    }

    sealed class UpdateProductsAction(val product: ProductAdapterModel.Product) {
        class AddToBasket(product: ProductAdapterModel.Product) : UpdateProductsAction(product)
        class Increase(product: ProductAdapterModel.Product) : UpdateProductsAction(product)
        class Decrease(product: ProductAdapterModel.Product) : UpdateProductsAction(product)
    }

    data class UpdateProductsResult(
        val action: UpdateProductsAction,
        val data: List<ProductAdapterModel>
    )
}