package com.example.test.domain.products.get

import com.example.test.domain.products.mapper.ToProductAdapterModelMapper
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetCategoriesWithProducts @Inject constructor(
    private val getCategories: GetCategories,
    private val getProducts: GetProducts,
    private val toProductAdapterModelMapper: ToProductAdapterModelMapper
) {
    operator fun invoke() = flow {
        val categories = getCategories()
        val products = getProducts()

        val result = toProductAdapterModelMapper.mapToProductAdapterModel(products, categories)
        emit(result)
    }
}