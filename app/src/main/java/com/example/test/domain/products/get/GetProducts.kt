package com.example.test.domain.products.get

import com.example.test.data.repositories.ProductsMockedRepository
import javax.inject.Inject

class GetProducts @Inject constructor(
    private val productsMockedRepository: ProductsMockedRepository
) {
    suspend operator fun invoke() = productsMockedRepository()
}