package com.example.test.domain.users

import com.example.test.data.databases.user.entities.User
import com.example.test.data.repositories.user.UserRepository
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SaveUser @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend operator fun invoke(login: String?, password: String?) = flow {
        try {
            if (login.isNullOrEmpty()) {
                emit(SaveUserResult.LoginNotValid)
                return@flow
            }

            if (password.isNullOrEmpty()) {
                emit(SaveUserResult.PasswordNoValid)
                return@flow
            }

            val user = User(login, password)
            userRepository.saveUser(user)

            emit(SaveUserResult.UserSaved)
        } catch (ex: Exception) {
            emit(SaveUserResult.Error(ex))
        }
    }

    sealed class SaveUserResult {
        object UserSaved : SaveUserResult()
        object LoginNotValid : SaveUserResult()
        object PasswordNoValid : SaveUserResult()

        data class Error(val ex: Exception) : SaveUserResult()
    }
}