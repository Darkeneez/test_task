package com.example.test.domain.products.get

import com.example.test.data.repositories.CategoriesMockedRepository
import javax.inject.Inject

class GetCategories @Inject constructor(
    private val categoriesMockedRepository: CategoriesMockedRepository
) {
    suspend operator fun invoke() = categoriesMockedRepository()
}