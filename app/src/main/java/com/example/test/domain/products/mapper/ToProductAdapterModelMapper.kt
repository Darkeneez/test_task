package com.example.test.domain.products.mapper

import com.example.test.data.entites.Category
import com.example.test.data.entites.Product
import com.example.test.data.entites.ProductAdapterModel
import javax.inject.Inject

class ToProductAdapterModelMapper @Inject constructor() {

    fun mapToProductAdapterModel(
        products: List<Product>,
        categories: List<Category>
    ): List<ProductAdapterModel> {
        val result = mutableListOf<ProductAdapterModel>()

        categories.forEach { category ->

            val categoryProducts = products
                .filter { product -> product.categoryId == category.categoryId }
                .map { ProductAdapterModel.Product(it) }

            result.add(ProductAdapterModel.Category(category))
            result.addAll(categoryProducts)
        }

        return result
    }
}