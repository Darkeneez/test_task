package com.example.test.domain.users

import com.example.test.data.databases.user.entities.User
import com.example.test.data.repositories.user.UserRepository
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetUser @Inject constructor(
    private val userRepository: UserRepository
) {
    operator fun invoke() = flow {
        try {
            val user = userRepository.getUser()

            val result = when (user == null) {
                true -> GetUserResult.NoUser
                false -> GetUserResult.UserIsValid(user)
            }

            emit(result)
        } catch (ex: Exception) {
            emit(GetUserResult.Error(ex))
        }
    }

    sealed class GetUserResult {
        data class UserIsValid(val user: User) : GetUserResult()
        data class Error(val exception: Exception) : GetUserResult()

        object NoUser : GetUserResult()
    }
}