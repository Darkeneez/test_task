package com.example.test.data.repositories.user

import com.example.test.data.databases.user.dao.UsersDao
import com.example.test.data.databases.user.entities.User
import javax.inject.Inject

class UserRepository @Inject constructor(private val usersDao: UsersDao) {

    suspend fun getUser(): User? {
        return usersDao.getUser()
    }

    suspend fun saveUser(user: User) {
        usersDao.insertUser(user)
    }
}