package com.example.test.data.repositories

import com.example.test.data.entites.Category
import kotlinx.coroutines.delay
import javax.inject.Inject

class CategoriesMockedRepository @Inject constructor() {

    private companion object {
        const val GET_CATEGORIES_DELAY = 2000L
    }

    suspend operator fun invoke(): List<Category> {
        delay(GET_CATEGORIES_DELAY)

        return listOf(
            Category(1, "Фрукты"),
            Category(2, "Напитки"),
            Category(3, "Бакалея")
        )
    }
}