package com.example.test.data.entites

data class Product(
    val id: Int,
    val name: String,
    val imageUrl: String,
    val categoryId: Int,
    val count: Int = 0
)