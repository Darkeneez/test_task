package com.example.test.data.databases.user

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.test.data.databases.user.dao.UsersDao
import com.example.test.data.databases.user.entities.User

@Database(entities = [User::class], version = 1)
abstract class UsersDataBase : RoomDatabase() {
    abstract fun getUsersDao(): UsersDao
}