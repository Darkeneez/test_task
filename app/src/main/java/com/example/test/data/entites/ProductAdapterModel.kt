package com.example.test.data.entites

typealias ECategory = Category
typealias EProduct = Product

sealed class ProductAdapterModel {
    data class Category(val category: ECategory) : ProductAdapterModel()
    data class Product(val product: EProduct, var addedToBasket: Boolean = false) :
        ProductAdapterModel()
}