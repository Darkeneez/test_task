package com.example.test.data.repositories

import com.example.test.data.entites.Product
import kotlinx.coroutines.delay
import javax.inject.Inject

class ProductsMockedRepository @Inject constructor() {

    private companion object {
        const val GET_PRODUCTS_DELAY = 2000L
    }

    suspend operator fun invoke(): List<Product> {
        delay(GET_PRODUCTS_DELAY)

        return listOf(
            Product(123, "Бананы сушеные", "/12/18/59", 1),
            Product(124, "Апельсины", "/11/73/82", 1),
            Product(125, "Лимон", "/10/91/14", 1),
            Product(126, "Вода", "/11/70/69", 2),
            Product(127, "Кола", "/10/00/86", 2),
            Product(128, "Спрайт", "/11/75/30", 2),
            Product(129, "Фанта", "/11/47/34", 2),
            Product(130, "Рис", "/10/29/51", 3)
        )
    }
}