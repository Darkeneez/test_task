package com.example.test.data.entites

data class Category(
    val categoryId: Int,
    val categoryName: String
)