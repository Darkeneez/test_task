package com.example.test.utils

import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.loadImage(address: String) {
    Picasso.with(context)
        .load("https://photos.5-delivery.ru/small$address")
        .into(this)
}
