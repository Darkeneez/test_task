package com.example.test.feature.main.recycler

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import com.example.test.R
import com.example.test.data.entites.ProductAdapterModel

class CategoryViewHolder(itemView: View) : ProductAdapter.ViewHolder(itemView) {

    override fun bind(productAdapterModel: ProductAdapterModel) {
        if (productAdapterModel is ProductAdapterModel.Category) {
            itemView.findViewById<AppCompatTextView>(R.id.tvCategory).text = productAdapterModel.category.categoryName
        }
    }
}
