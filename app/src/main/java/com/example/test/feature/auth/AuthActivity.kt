package com.example.test.feature.auth

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.ViewModelProvider
import com.example.test.feature.main.MainActivity
import com.example.test.R
import com.example.test.domain.users.SaveUser

class AuthActivity : AppCompatActivity() {

    private lateinit var viewModel: AuthViewModel

    private lateinit var btnSave: AppCompatButton
    private lateinit var etLogin: EditText
    private lateinit var etPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.auth_activity)

        viewModel = ViewModelProvider(this).get(AuthViewModel::class.java)
        viewModel.saveUserLiveData.observe(this) { result ->
            when (result) {
                is SaveUser.SaveUserResult.PasswordNoValid -> onPasswordNotValid()
                is SaveUser.SaveUserResult.LoginNotValid -> onLoginNotValid()
                is SaveUser.SaveUserResult.UserSaved -> onUserSaved()
                is SaveUser.SaveUserResult.Error -> onSaveUserError(result.ex)
            }
        }

        getViews()
        setListeners()
    }

    private fun getViews() {
        btnSave = findViewById(R.id.btnSave)
        etLogin = findViewById(R.id.etLogin)
        etPassword = findViewById(R.id.etPassword)
    }

    private fun setListeners() {
        btnSave.setOnClickListener {
            val login = etLogin.text.toString()
            val password = etPassword.text.toString()

            viewModel.saveNewUser(login, password)
        }
    }

    private fun onPasswordNotValid() {
        Toast.makeText(this, "Введите пароль", Toast.LENGTH_LONG).show()
    }

    private fun onLoginNotValid() {
        Toast.makeText(this, "Введите логин", Toast.LENGTH_LONG).show()
    }

    private fun onUserSaved() {
        val intent = Intent(this, MainActivity::class.java)

        startActivity(intent)
        finish()
    }

    private fun onSaveUserError(ex: Exception) {
        Toast.makeText(this, ex.message, Toast.LENGTH_LONG).show()
    }
}