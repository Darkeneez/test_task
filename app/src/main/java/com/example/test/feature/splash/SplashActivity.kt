package com.example.test.feature.splash

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.test.R
import com.example.test.domain.users.GetUser
import com.example.test.feature.auth.AuthActivity
import com.example.test.feature.main.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)

        ViewModelProvider(this)
            .get(SplashViewModel::class.java)
            .userLiveData
            .observe(this) { result ->
                when (result) {
                    is GetUser.GetUserResult.NoUser -> navigateToAuth()
                    is GetUser.GetUserResult.UserIsValid -> navigateToMain()
                    is GetUser.GetUserResult.Error -> Toast.makeText(
                        this,
                        "exception",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }

    private fun navigateToAuth() {
        val intent = Intent(this, AuthActivity::class.java)

        startActivity(intent)
        finish()
    }

    private fun navigateToMain() {
        val intent = Intent(this, MainActivity::class.java)

        startActivity(intent)
        finish()
    }
}