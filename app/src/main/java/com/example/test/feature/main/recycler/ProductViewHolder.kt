package com.example.test.feature.main.recycler

import android.view.View
import androidx.core.view.isInvisible
import com.example.test.data.entites.ProductAdapterModel
import com.example.test.utils.loadImage
import kotlinx.android.synthetic.main.item_product.view.*

class ProductViewHolder(
    itemView: View,
    private val productListener: ProductListener
) : ProductAdapter.ViewHolder(itemView) {
    override fun bind(productAdapterModel: ProductAdapterModel) {

        if (productAdapterModel !is ProductAdapterModel.Product) {
            return
        }

        val item = productAdapterModel.product

        with(itemView) {
            tvProductName?.text = item.name
            tvProductCount?.text = item.count.toString()

            ivProductImage?.loadImage(item.imageUrl)

            ibPlus?.setOnClickListener {
                productListener.onIncrease(productAdapterModel)
            }

            ivMinus?.setOnClickListener {
                productListener.onDecrease(productAdapterModel)
            }

            btnAddToBasket?.setOnClickListener {
                productListener.onAddToBasket(productAdapterModel)
            }

            btnAddToBasket?.isInvisible = productAdapterModel.addedToBasket

            ibPlus?.isInvisible = !productAdapterModel.addedToBasket || item.count == 0
            ivMinus?.isInvisible = !productAdapterModel.addedToBasket || item.count == 0

            tvProductCount?.isInvisible = !productAdapterModel.addedToBasket
        }
    }
}