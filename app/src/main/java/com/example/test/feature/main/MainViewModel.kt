package com.example.test.feature.main

import androidx.lifecycle.*
import com.example.test.data.entites.ProductAdapterModel
import com.example.test.domain.products.get.GetCategoriesWithProducts
import com.example.test.domain.products.update.UpdateProducts
import com.example.test.domain.products.update.UpdateProducts.UpdateProductsAction.*
import com.example.test.feature.application.TestApplication.Companion.di
import com.example.test.feature.main.recycler.ProductListener
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel : ViewModel(), ProductListener {
    init {
        di.inject(this)
    }

    @Inject
    lateinit var getCategoriesWithProducts: GetCategoriesWithProducts

    @Inject
    lateinit var updateProducts: UpdateProducts

    private var currentProducts: List<ProductAdapterModel> = arrayListOf()

    private val _categoriesWithProductsData = getCategoriesWithProducts().asLiveData()
    private val _updateProductsLiveData = MutableLiveData<UpdateProducts.UpdateProductsResult>()

    val productAdapterModelData: LiveData<List<ProductAdapterModel>>
        get() = _categoriesWithProductsData.map {
            currentProducts = it
            it
        }
    val updateProductsLiveData: LiveData<UpdateProducts.UpdateProductsResult> get() = _updateProductsLiveData

    override fun onIncrease(product: ProductAdapterModel.Product) {
        viewModelScope.launch {
            updateProducts.updateProduct(currentProducts, Increase(product)).collect {
                currentProducts = it.data
                _updateProductsLiveData.value = it
            }
        }
    }

    override fun onDecrease(product: ProductAdapterModel.Product) {
        viewModelScope.launch {
            updateProducts.updateProduct(currentProducts, Decrease(product)).collect {
                currentProducts = it.data
                _updateProductsLiveData.value = it
            }
        }
    }

    override fun onAddToBasket(product: ProductAdapterModel.Product) {
        viewModelScope.launch {
            updateProducts.updateProduct(currentProducts, AddToBasket(product)).collect {
                currentProducts = it.data
                _updateProductsLiveData.value = it
            }
        }
    }
}