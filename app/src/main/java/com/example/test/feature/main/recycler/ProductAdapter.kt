package com.example.test.feature.main.recycler

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.test.R
import com.example.test.data.entites.ProductAdapterModel

class ProductAdapter(
    private val productListener: ProductListener
) : ListAdapter<ProductAdapterModel, ProductAdapter.ViewHolder>(EqualsDiffCallback { a, b -> a == b }) {

    companion object {
        private const val CATEGORY_HOLDER = 0
        private const val PRODUCT_HOLDER = 1
    }

    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is ProductAdapterModel.Category -> CATEGORY_HOLDER
        else -> PRODUCT_HOLDER
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder = when (type) {
        CATEGORY_HOLDER -> getCategoryHolder(parent)
        else -> getProductHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    private fun getCategoryHolder(parent: ViewGroup): ViewHolder {
        return CategoryViewHolder(getLayout(parent, R.layout.item_category))
    }

    private fun getProductHolder(parent: ViewGroup): ViewHolder {
        return ProductViewHolder(getLayout(parent, R.layout.item_product), productListener)
    }

    private fun getLayout(parent: ViewGroup, @LayoutRes resId: Int): View {
        return LayoutInflater.from(parent.context).inflate(resId, parent, false)
    }

    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(productAdapterModel: ProductAdapterModel)
    }
}
