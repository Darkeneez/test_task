package com.example.test.feature.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.test.R
import com.example.test.feature.main.recycler.ProductAdapter
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        adapter = ProductAdapter(viewModel)
        rvItems?.adapter = adapter

        setObservers()
    }

    private fun setObservers() {
        viewModel.productAdapterModelData.observe(this) { result ->
            adapter.submitList(result)
        }
        viewModel.updateProductsLiveData.observe(this) { result ->
            val data = result.data
            adapter.submitList(data)
        }
    }
}