package com.example.test.feature.application

import android.app.Application
import com.example.test.di.AppComponent
import com.example.test.di.DaggerAppComponent

class TestApplication : Application() {
    companion object {
        lateinit var di: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        di = DaggerAppComponent.builder()
            .app(this)
            .build()
    }
}