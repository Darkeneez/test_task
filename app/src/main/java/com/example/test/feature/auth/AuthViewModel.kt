package com.example.test.feature.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.test.domain.users.SaveUser
import com.example.test.feature.application.TestApplication.Companion.di
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class AuthViewModel : ViewModel() {

    init {
        di.inject(this)
    }

    @Inject
    lateinit var saveUser: SaveUser

    private val _saveUserLiveData = MutableLiveData<SaveUser.SaveUserResult>()

    val saveUserLiveData: LiveData<SaveUser.SaveUserResult> get() = _saveUserLiveData

    fun saveNewUser(login: String?, password: String?) {
        viewModelScope.launch {
            saveUser(login, password).collect {
                _saveUserLiveData.postValue(it)
            }
        }
    }
}