package com.example.test.feature.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.example.test.domain.users.GetUser
import com.example.test.feature.application.TestApplication.Companion.di
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class SplashViewModel : ViewModel() {

    init {
        di.inject(this)
    }

    @Inject
    lateinit var getUser: GetUser

    val userLiveData: LiveData<GetUser.GetUserResult> = getUser()
        .onStart { }
        .asLiveData(Dispatchers.IO)
}