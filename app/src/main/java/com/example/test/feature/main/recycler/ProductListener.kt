package com.example.test.feature.main.recycler

import com.example.test.data.entites.ProductAdapterModel

interface ProductListener {
    fun onIncrease(product: ProductAdapterModel.Product)

    fun onDecrease(product: ProductAdapterModel.Product)

    fun onAddToBasket(product: ProductAdapterModel.Product)
}