package com.example.test.di.modules

import android.content.Context
import androidx.room.Room
import com.example.test.data.databases.user.UsersDataBase
import com.example.test.data.databases.user.dao.UsersDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    private companion object {
        const val USERS_DB_NAME = "users"
    }

    @Provides
    @Singleton
    fun provideUsersDataBase(context: Context): UsersDataBase = Room.databaseBuilder(
        context,
        UsersDataBase::class.java,
        USERS_DB_NAME
    ).build()

    @Provides
    @Singleton
    fun provideUsersDao(usersDataBase: UsersDataBase): UsersDao = usersDataBase.getUsersDao()
}
