package com.example.test.di

import com.example.test.di.modules.AppModule
import com.example.test.di.modules.DatabaseModule
import com.example.test.feature.application.TestApplication
import com.example.test.feature.auth.AuthViewModel
import com.example.test.feature.main.MainViewModel
import com.example.test.feature.splash.SplashViewModel
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DatabaseModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun app(app: TestApplication): Builder

        fun build(): AppComponent
    }

    fun inject(splashViewModel: SplashViewModel)
    fun inject(authViewModel: AuthViewModel)
    fun inject(mainViewModel: MainViewModel)
}