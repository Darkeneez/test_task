package com.example.test.di.modules

import android.app.Application
import android.content.Context
import com.example.test.feature.application.TestApplication
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideApplication(app: TestApplication): Application = app

    @Provides
    fun provideApplicationContext(app: Application): Context = app
}